<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\billwerk_subscriptions\Environment;
use Drupal\user\RoleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Billwerk Subscriptions settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface|null $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typedConfigManager,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($config_factory, $typedConfigManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'billwerk_subscriptions_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['billwerk_subscriptions.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('billwerk_subscriptions.settings');

    $form['settings'] = [
      '#type' => 'details',
      '#title' => $this->t('General settings'),
      '#open' => TRUE,
      '#description' => $this->t('General Billwerk Subscriptions settings'),
      '#description_display' => 'after',
    ];
    $form['settings']['environment'] = [
      '#type' => 'radios',
      '#title' => $this->t('Current Billwerk Environment'),
      '#options' => Environment::getEnvironmentsSelectOptions(),
      '#default_value' => $config->get('environment'),
      '#description' => $this->t('Select the Billwerk environment to use for this project currently. This should typically never be switched to Sandbox when running in production and vice-versa. Mind that the Billwerk IDs differ per environment!'),
    ];
    $roles_options = array_map(fn(RoleInterface $role) => $role->label(), $this->entityTypeManager->getStorage('user_role')->loadMultiple());

    // Remove options that should never be selectable:
    unset($roles_options['authenticated'], $roles_options['administrator'], $roles_options['anonymous']);
    $form['settings']['billwerk_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Billwerk roles'),
      '#options' => array_map('\Drupal\Component\Utility\Html::escape', $roles_options),
      '#default_value' => $config->get('billwerk_roles'),
      '#description' => $this->t('Select the roles that should be (un)assigned by Billwerk Subscriptions. <strong>Important:</strong> Only use <em>dedicated roles</em> and <em>never (un)assign these manually</em> in Drupal, as this will be overwritten!'),
    ];
    $form['settings']['logging_levels'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Logging levels'),
      '#options' => [
        'emergency' => $this->t('Emergency'),
        'alert' => $this->t('Alert'),
        'critical' => $this->t('Critical'),
        'error' => $this->t('Error'),
        'warning' => $this->t('Warning'),
        'notice' => $this->t('Notice'),
        'info' => $this->t('Info'),
        'debug' => $this->t('Debug'),
      ],
      '#default_value' => $config->get('logging_levels'),
      '#description' => $this->t('Select the logging levels to use.'),
    ];
    $form['settings']['api_response_cache_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Cache API responses'),
      '#default_value' => $config->get('api_response_cache_enabled'),
      '#description' => $this->t('Cache API responses during script execution <em>(drupal_static())</em>. Should typically be enabled to reduce API calls.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('billwerk_subscriptions.settings')
      ->set('environment', $form_state->getValue('environment'))
      ->set('billwerk_roles', array_filter($form_state->getValue('billwerk_roles', [])))
      ->set('logging_levels', array_filter($form_state->getValue('logging_levels', [])))
      ->set('api_response_cache_enabled', $form_state->getValue('api_response_cache_enabled'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}

<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions\Exception;

/**
 * API-related Exception.
 */
class WebhookException extends \Exception {

}

<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions\DataObject;

/**
 * The Billwerk Subscriptions Data Object (DTO).
 *
 * This represents the subscription part of a Billwerk Contract.
 * It is based on the call: `/api/v1/contracts/{id}/subscriptions`:
 * "Retrieves all currently active subscriptions including plan variant,
 * component subscriptions and discount subscriptions"
 *
 * We currently do not handle discount subscriptions, as they are not relevant
 * for our processes.
 *
 * Only contains the subset of fields relevant for the module's typical cases.
 * If you need additional values, consider requesting them from the API
 * or open an issue with good enough reasons. ;)
 *
 * Example (https://swagger.billwerk.io/#/operations/Contracts/Contracts_GetSubscriptions_id_timestamp_GET):
 * {
 *   "Id": "658d0f35c703e6a4d126700c",
 *   "Phase": {
 *     "Type": "Normal",
 *     "StartDate": "2023-06-28T06:01:25.3279072Z",
 *     "PlanVariantId": "5b20d2df81b1f00d1425280f",
 *     "PlanId": "5b20d2deba5c1e13409b15f2",
 *     "Quantity": 1,
 *     "InheritStartDate": false
 *   },
 *   "ComponentSubscriptions": [
 *     {
 *       "Id": "599d51f881b1f00a28f7ae9n",
 *       "ContractId": "599d51f881b1f00a28f7ae14",
 *       "CustomerId": "599d51f881b1f00a28f7asdf",
 *       "ComponentId": "599d51f881b1f00a28f7ae10",
 *       "Quantity": 1,
 *       "StartDate": "2023-11-28T06:01:25.3279034Z",
 *       "BilledUntil": "2024-01-28T06:01:25.3279013Z",
 *       "Status": "Active",
 *       "EndDate": "2024-06-28T06:01:25.3279030Z",
 *       "Memo": "Memoexample"
 *     },
 *     {
 *       "Id": "599d51f881b1f00a28f7ae8n",
 *       "ContractId": "599d51f881b1f00a28f7ae14",
 *       "CustomerId": "599d51f881b1f00a28f7asdf",
 *       "ComponentId": "599d51f881b1f00a28f7ae11",
 *       "Quantity": 0,
 *       "StartDate": "2023-11-28T06:01:25.3279049Z",
 *       "BilledUntil": "2024-01-28T06:01:25.3279046Z",
 *       "Status": "Active",
 *       "EndDate": "2024-06-28T06:01:25.3279047Z",
 *       "Memo": "Memoexample"
 *     }
 *   ],
 *   "DiscountSubscriptions": [
 *     {
 *       "Id": "599d51f881b1f00a28f7ae2m",
 *       "ContractId": "599d51f881b1f00a28f7ae14",
 *       "CouponCode": "50off",
 *       "CouponId": "599d51f881b1f00a28f7ae16",
 *       "DiscountId": "599d51f881b1f00a28f7ae18",
 *       "StartDate": "2023-12-28T06:01:25.3279061Z",
 *       "Status": "Active",
 *       "EndDate": "2024-06-28T06:01:25.3279060Z"
 *     },
 *     {
 *       "Id": "599d51f881b1f00a28f7ae2o",
 *       "ContractId": "599d51f881b1f00a28f7ae14",
 *       "CouponCode": "starter",
 *       "CouponId": "599d51f881b1f00a28f7ae17",
 *       "DiscountId": "599d51f881b1f00a28f7ae20",
 *       "StartDate": "2023-12-28T06:01:25.3279066Z",
 *       "Status": "Active",
 *       "EndDate": "2024-01-28T06:01:25.3279065Z"
 *     }
 *   ]
 * }
 *
 * @improve:
 * Addition:
 * Due to the limitations of the incomplete values returned by billwerk,
 * we fill additional parameters with a further contract details API call.
 * see BillwerkDataObjectFactory for details.
 */
final class BillwerkContractSubscription {
  const PHASE_TYPE_NORMAL = 'Normal';
  const PHASE_TYPE_INACTIVE = 'Inactive';
  /**
   * Note that this is not affected by pausing. This value stays the same!
   */
  const PHASE_TYPE_TRIAL = 'Trial';

  /**
   * Constructor.
   *
   * @param string $phaseType
   *   The phase type of the subscription.
   * @param string $planVariantId
   *   The plan variant id of the subscription.
   * @param string $planId
   *   The plan id of the subscription.
   * @param int $quantity
   *   The quantity of the subscription.
   * @param ?string $startDate
   *   The start date of the subscription.
   * @param ?string $endDate
   *   The end date of the subscription.
   * @param ?string $trialEndDate
   *   The trial end date of the subscription.
   * @param ?string $billedUntil
   *   The billed until date of the subscription.
   * @param ?string $lastBillingDate
   *   The last billing date of the subscription.
   * @param ?string $nextBillingDate
   *   The next billing date of the subscription.
   * @param ?string $currency
   *   The currency of the subscription.
   * @param ?float $balance
   *   The balance of the subscription.
   * @param array $componentSubscriptions
   *   The component subscriptions of the subscription.
   */
  public function __construct(
    protected readonly string $phaseType,
    protected readonly string $planVariantId,
    protected readonly string $planId,
    protected readonly int $quantity,
    protected readonly ?string $startDate,
    protected readonly ?string $endDate,
    protected readonly ?string $trialEndDate,
    protected readonly ?string $billedUntil,
    protected readonly ?string $lastBillingDate,
    protected readonly ?string $nextBillingDate,
    protected readonly ?string $currency,
    protected readonly ?float $balance,
    protected readonly array $componentSubscriptions,
  ) {
  }

  /**
   * Returns this Billwerk Contract Subscription phase type.
   *
   * @return string
   *   The phase type of the subscription.
   */
  public function getPhaseType(): string {
    return $this->phaseType;
  }

  /**
   * Returns this Billwerk Contract Subscription plan variant id.
   *
   * @return string
   *   The plan variant id of the subscription.
   */
  public function getPlanVariantId(): string {
    return $this->planVariantId;
  }

  /**
   * Returns this Billwerk Contract Subscription plan id.
   *
   * @return string
   *   The plan id of the subscription.
   */
  public function getPlanId(): string {
    return $this->planId;
  }

  /**
   * Returns this Billwerk Contract Subscription quantity.
   *
   * @return int
   *   The quantity of the subscription.
   */
  public function getQuantity(): int {
    return $this->quantity;
  }

  /**
   * Returns the StartDate.
   *
   * @return string|null
   *   The start date of the subscription.
   */
  public function getStartDate(): ?string {
    return $this->startDate;
  }

  /**
   * Returns the EndDate.
   *
   * @return ?string
   *   The end date of the subscription.
   */
  public function getEndDate(): ?string {
    return $this->endDate;
  }

  /**
   * Returns the TrialEndDate.
   *
   * @return ?string
   *   The trial end date of the subscription.
   */
  public function getTrialEndDate(): ?string {
    return $this->trialEndDate;
  }

  /**
   * Returns the BilledUntil date.
   *
   * @return ?string
   *   The billed until date of the subscription.
   */
  public function getBilledUntil(): ?string {
    return $this->billedUntil;
  }

  /**
   * Returns the LastBillingDate.
   *
   * @return ?string
   *   The last billing date of the subscription.
   */
  public function getLastBillingDate(): ?string {
    return $this->lastBillingDate;
  }

  /**
   * Returns the NextBillingDate.
   *
   * @return ?string
   *   The next billing date of the subscription.
   */
  public function getNextBillingDate(): ?string {
    return $this->nextBillingDate;
  }

  /**
   * Returns the Balance.
   *
   * @return ?float
   *   The balance of the subscription.
   */
  public function getBalance(): ?float {
    return $this->balance;
  }

  /**
   * Returns component subscriptions.
   *
   * Returns this Billwerk Contract Subscription component subscriptions
   * (Add-on's).
   *
   * @return array
   *   The component subscriptions of the subscription.
   */
  public function getComponentSubscriptions(): array {
    return $this->componentSubscriptions;
  }

  /**
   * Returns the Currency.
   *
   * @return ?string
   *   The currency of the subscription.
   */
  public function getCurrency(): ?string {
    return $this->currency;
  }

}

<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions;

use Drupal\billwerk_subscriptions\Exception\ApiException;

/**
 * Helper class to build Billwerk provided embeds, esp. for self-service.
 */
final class EmbedHelper {

  /**
   * Constructs an EmbedHelper object.
   */
  public function __construct(
    protected readonly Api $api,
  ) {
  }

  /**
   * Build self service manage contract embed.
   *
   * Builds and returns a render array element with an iframe to embed the
   * self service manage contract page for the given $subscriber.
   *
   * @param \Drupal\billwerk_subscriptions\Subscriber $subscriber
   *   The subscriber to build the embed for.
   *
   * @return array
   *   Render array for displaying the iframe embed.
   *
   * @throws \Drupal\billwerk_subscriptions\Exception\ApiException
   *   If the self service URL could not be retrieved.
   */
  public function buildSelfserviceManageContractEmbed(Subscriber $subscriber): array {
    if (!$subscriber->hasUserBillwerkContractId()) {
      return [];
    }
    $billwerkContractId = $subscriber->getUserBillwerkContractId();
    $selfserviceUrl = $this->api->getSelfserviceUrl($billwerkContractId);
    if (empty($selfserviceUrl)) {
      throw new ApiException("Selfservice Url could not be retrieved for Billwerk Contract #{$billwerkContractId}");
    }
    return $this->buildEmbed($selfserviceUrl);
  }

  // @codingStandardsIgnoreStart
  // @improve: If anyone needs it, we could also add an embed function here
  // to provide the Billwerk Signup iframe.
  // @see https://developer.billwerk.io/docs/gettingStarted/integratingBillwerk#hosted-customer-self-service
  // But typically that iframe
  // is not very helpful in Drupal environments and processes.
  // For reference see subman module: SubmanEmbedsHelper.php
  // @codingStandardsIgnoreEnd

  /**
   * Builds and returns a render array element.
   *
   * Builds and returns a render array element with an iframe to embed the self
   * service url.
   *
   * @param string $url
   *   The URL to show on the iframe embed.
   *
   * @return array
   *   Render array for displaying the iframe embed.
   */
  protected function buildEmbed(string $url): array {
    $build = [
      '#type' => 'inline_template',
      '#template' => '<div class="billwerk-subscriptions-embed--wrapper embed-responsive"><iframe class="billwerk-subscriptions-embed" src="{{ url }}" border="0" loading="lazy"></iframe></div>',
      '#cache' => ['max-age' => 0],
      '#context' => [
        'url' => $url,
      ],
      '#attached' => [
        'library' => ['billwerk_subscriptions/embed_iframe'],
      ],
    ];

    return $build;
  }

}

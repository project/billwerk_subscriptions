<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Action\ActionBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\billwerk_subscriptions\Subscriber;
use Drupal\user\UserInterface;

/**
 * Provides a Refresh Drupal user subscription from Billwerk action.
 *
 * (e.g. roles, profile data, ...)
 *
 * @Action(
 *   id = "billwerk_user_refresh_subscription_action",
 *   label = @Translation("Refresh Drupal user subscription (roles, profile data, ...) from Billwerk"),
 *   type = "user",
 *   category = @Translation("Billwerk Subscriptions"),
 * )
 *
 * @DCG
 * For updating entity fields consider extending FieldUpdateActionBase.
 * @see \Drupal\Core\Field\FieldUpdateActionBase
 *
 * @DCG
 * In order to set up the action through admin interface the plugin has to be
 * configurable.
 * @see https://www.drupal.org/project/drupal/issues/2815301
 * @see https://www.drupal.org/project/drupal/issues/2815297
 *
 * @DCG
 * The whole action API is subject of change.
 * @see https://www.drupal.org/project/drupal/issues/2011038
 */
final class BillwerkUserRefreshSubscriptionAction extends ActionBase {

  /**
   * {@inheritdoc}
   */
  public function access($entity, ?AccountInterface $account = NULL, $return_as_object = FALSE): AccessResultInterface|bool {
    /** @var \Drupal\user\UserInterface $entity */
    $access = $entity->access('update', $account, TRUE)
      ->andIf(AccessResult::allowedIfHasPermission($account, 'billwerk_subscriptions_manual_refresh_user_subscriptions'));
    return $return_as_object ? $access : $access->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function execute(?UserInterface $user = NULL): void {
    if ($user !== NULL) {
      $subscriber = Subscriber::load($user);
      $subscriber->refreshFromBillwerkContractSubscription();
    }
  }

}

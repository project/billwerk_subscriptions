<?php

namespace Drupal\billwerk_subscriptions\Event;

use Drupal\billwerk_subscriptions\Subscriber;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event, fired if a Drupal user's Billwerk contract change was detected.
 *
 * This is a high level event.
 *
 * This event is fired when a change to a User's Billwerk Contract was
 * detected, so that you can react to this in Drupal without
 * having to mess around with the lower level webhooks.
 *
 * Also consider using the SubscriberRefreshUserEvent, which should be
 * used to execute changes to the user account instead of this
 * specific event.
 *
 * This will not be called, if a new contract is created or a contract is
 * entirely deleted.
 */
class SubscriberContractChangedEvent extends Event {

  const CONTRACT_CHANGE_TYPE_SIGNUP = 'Signup';
  const CONTRACT_CHANGE_TYPE_UPGRADE = 'Upgrade';
  const CONTRACT_CHANGE_TYPE_TRIALENDCHANGE = 'TrialEndChange';
  const CONTRACT_CHANGE_TYPE_COMPONENTSUBSCRIPTIONCHANGE = 'ComponentSubscriptionChange';
  const CONTRACT_CHANGE_TYPE_DISCOUNTSUBSCRIPTIONCHANGE = 'DiscountSubscriptionChange';
  const CONTRACT_CHANGE_TYPE_TIMEBASED = 'Timebased';
  const CONTRACT_CHANGE_TYPE_ENDCONTRACT = 'EndContract';
  const CONTRACT_CHANGE_TYPE_ANNULATION = 'Annulation';
  const CONTRACT_CHANGE_TYPE_PAUSE = 'Pause';
  const CONTRACT_CHANGE_TYPE_RESUME = 'Resume';
  const CONTRACT_CHANGE_TYPE_EXTERNALSUBSCRIPTIONSYNC = 'ExternalSubscriptionSync';

  /**
   * Constructor.
   *
   * @param \Drupal\billwerk_subscriptions\Subscriber $subscriber
   *   The subscriber.
   * @param string $changeType
   *   The name of the change type. Depends on the event and should typically
   *   be used from constants.
   */
  public function __construct(
    protected readonly Subscriber $subscriber,
    protected readonly string $changeType,
  ) {
    if (!in_array($changeType, [
      self::CONTRACT_CHANGE_TYPE_UPGRADE,
      self::CONTRACT_CHANGE_TYPE_TRIALENDCHANGE,
      self::CONTRACT_CHANGE_TYPE_COMPONENTSUBSCRIPTIONCHANGE,
      self::CONTRACT_CHANGE_TYPE_DISCOUNTSUBSCRIPTIONCHANGE,
      self::CONTRACT_CHANGE_TYPE_TIMEBASED,
      self::CONTRACT_CHANGE_TYPE_ENDCONTRACT,
      self::CONTRACT_CHANGE_TYPE_ANNULATION,
      self::CONTRACT_CHANGE_TYPE_PAUSE,
      self::CONTRACT_CHANGE_TYPE_RESUME,
      self::CONTRACT_CHANGE_TYPE_EXTERNALSUBSCRIPTIONSYNC,
    ])) {
      throw new \UnexpectedValueException("Unknown contract change type given: \"{$changeType}\"");
    }
  }

  /**
   * Gets the Subscriber whose Contract data changed.
   *
   * @return \Drupal\billwerk_subscriptions\Subscriber
   *   The subscriber.
   */
  public function getSubscriber(): Subscriber {
    return $this->subscriber;
  }

  /**
   * Returns the change type.
   *
   * @return string
   *   The change type string (class constant).
   */
  public function getChangeType(): string {
    return $this->changeType;
  }

}

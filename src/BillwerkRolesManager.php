<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\user\RoleStorageInterface;

/**
 * Helper manager class for interact with the Billwerk related Drupal roles.
 */
final class BillwerkRolesManager {

  /**
   * The settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The role storage.
   *
   * @var \Drupal\user\RoleStorageInterface
   */
  protected RoleStorageInterface $roleStorage;

  /**
   * Constructs a BillwerkRolesManager object.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    EntityTypeManagerInterface $entityTypeManager,
  ) {
    $this->config = $configFactory->get('billwerk_subscriptions.settings');
    $this->roleStorage = $entityTypeManager->getStorage('user_role');
  }

  /**
   * Returns the Billwerk handled Role ID's as array from the settings.
   *
   * We're not returning role objects, as Drupal does it the same way.
   *
   * @return array
   *   The Billwerk handled Role ID's as array.
   */
  public function getBillwerkRoles(): array {
    // Also set the values as keys to make sure it's unique and easier to
    // interact with.
    $billwerkRoles = $this->config->get('billwerk_roles');
    return array_combine($billwerkRoles, $billwerkRoles);
  }

  /**
   * Returns the Billwerk handled Role ID's as array from the settings.
   *
   * We're not returning role objects, as Drupal does it the same way.
   *
   * @return array
   *   The Billwerk handled Role options.
   */
  public function getBillwerkRolesSelectOptions(): array {
    // Also set the values as keys to make sure it's unique and easier to
    // interact with.
    $billwerkRoles = $this->config->get('billwerk_roles');
    $roleEntities = $this->roleStorage->loadMultiple($billwerkRoles);
    $rolesOptions = [];
    foreach ($roleEntities as $roleEntity) {
      $rolesOptions[$roleEntity->id()] = $roleEntity->label();
    }
    return $rolesOptions;
  }

}

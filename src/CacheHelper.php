<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Cache helper that wraps the default cache for billwerk_subscriptions caching.
 */
final class CacheHelper {

  const CACHE_STORE_PREFIX = 'billwerk_subscriptions_store:';

  /**
   * Constructs a CacheHelper object.
   */
  public function __construct(
    private readonly CacheBackendInterface $cacheDefault,
  ) {
  }

  /**
   * Stores given value under the given key in the default cache.
   *
   * @param string $key
   *   The key to store the data with.
   * @param mixed $data
   *   The data to store.
   * @param int $expire
   *   The unix timestamp after which the data is considered invalid (permanent,
   *   if nothing specified).
   */
  public function save(string $key, mixed $data, int $expire = Cache::PERMANENT): void {
    $this->cacheDefault->set(self::CACHE_STORE_PREFIX . $key, $data, $expire);
  }

  /**
   * Retrieves the stored value for the given key from the default cache.
   *
   * @param string $key
   *   The key to look for.
   *
   * @return mixed
   *   The cached data or NULL if not found.
   */
  public function load(string $key): mixed {
    $cache = $this->cacheDefault->get(self::CACHE_STORE_PREFIX . $key);
    return $cache ? $cache->data : NULL;
  }

  /**
   * Clears the cached data from cache for the given key.
   *
   * @param string $key
   *   The key to clear the cache for.
   */
  public function delete(string $key): void {
    $this->cacheDefault->delete(self::CACHE_STORE_PREFIX . $key);
  }

}

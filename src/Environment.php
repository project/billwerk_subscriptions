<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions;

use Drupal\billwerk_subscriptions\Exception\EnvironmentException;

/**
 * Helper class to provide environment related functionality.
 *
 * Environment can be "Production" or "Sandbox" and therefore has implications
 * on the different URLs to use. Furthermore the environments have different
 * IDs for their specific data.
 */
final class Environment {

  const API_PRODUCTION_URL = 'https://app.billwerk.com/';
  const API_PRODUCTION_SELFSERVICE_URL = 'https://selfservice.billwerk.com/';
  const API_SANDBOX_URL = 'https://sandbox.billwerk.com/';
  const API_SANDBOX_SELFSERVICE_URL = 'https://selfservice.sandbox.billwerk.com/';
  const API_OAUTH_PATH = 'oauth/token';
  const API_BASE_PATH = 'api/v1/';
  const API_CONTRACTS_RESOURCE_PATH = 'api/v1/contracts';
  const API_CONTRACTCHANGES_RESOURCE_PATH = 'api/v1/contractchanges';
  const API_CUSTOMERS_RESOURCE_PATH = 'api/v1/customers';
  const API_CUSTOMERS_SEARCH_RESOURCE_PATH = 'api/v1/customers';
  const API_DISCOUNTS_RESOURCE_PATH = 'api/v1/discounts';
  const API_INVOICEDRAFTS_RESOURCE_PATH = 'api/v1/invoicedrafts';
  const API_INVOICES_RESOURCE_PATH = 'api/v1/invoices';
  const API_ORDERS_RESOURCE_PATH = 'api/v1/orders';
  const API_PLANGROUPS_RESOURCE_PATH = 'api/v1/plangroups';
  const API_PLANS_RESOURCE_PATH = 'api/v1/plans';
  const API_PLANVARIANTS_RESOURCE_PATH = 'api/v1/planvariants';
  const API_PRICELISTS_RESOURCE_PATH = 'api/v1/pricelists';
  const API_PRODUCTINFO_RESOURCE_PATH = 'api/v1/productinfo';
  const API_SUBSCRIPTIONS_RESOURCE_PATH = 'api/v1/subscriptions';
  const API_WEBHOOKS_RESOURCE_PATH = 'api/v1/webhooks';

  const ENVIRONMENT_PRODUCTION = 'production';

  const ENVIRONMENT_SANDBOX = 'sandbox';

  /**
   * Constructs an Environment object.
   */
  public function __construct(
    protected readonly SettingsHelper $settingsHelper,
  ) {
  }

  /**
   * Returns the current environment name string.
   *
   * Environment::ENVIRONMENT_PRODUCTION
   * or
   * Environment::ENVIRONMENT_SANDBOX.
   *
   * @throws \Drupal\billwerk_subscriptions\Exception\EnvironmentException
   *
   * @return string
   *   The environment name.
   */
  public function getEnvironmentName(): string {
    $environment = $this->settingsHelper->getEnvironment();
    if (!in_array($environment, self::getEnvironments())) {
      throw new EnvironmentException("The configured environment \"{$environment}\" is not allowed.");
    }
    return $environment;
  }

  /**
   * Returns the environments as array.
   *
   * @return array
   *   The environment constants.
   */
  public static function getEnvironments(): array {
    return [
      self::ENVIRONMENT_SANDBOX,
      self::ENVIRONMENT_PRODUCTION,
    ];
  }

  /**
   * Returns the environments as FAPI select #options array.
   *
   * @return array
   *   The environment constants as FAPI select #options array.
   */
  public static function getEnvironmentsSelectOptions(): array {
    $environments = self::getEnvironments();
    return array_combine($environments, array_map('ucfirst', $environments));
  }

  /**
   * Summary of getClientId.
   *
   * @return string
   *   The client ID.
   */
  public function getClientId(): string {
    return $this->isProduction() ? $this->settingsHelper->getProductionClientId() : $this->settingsHelper->getSandboxClientId();
  }

  /**
   * Summary of getClientSecret.
   *
   * @return string
   *   The client secret.
   */
  public function getClientSecret(): string {
    return $this->isProduction() ? $this->settingsHelper->getProductionClientSecret() : $this->settingsHelper->getSandboxClientSecret();
  }

  /**
   * Summary of getApiUrl.
   *
   * @param string $path
   *   The path to get the api url for.
   *
   * @return string
   *   The api url.
   */
  public function getApiUrl(string $path = ''): string {
    $baseUrl = $this->isProduction() ? self::API_PRODUCTION_URL : self::API_SANDBOX_URL;
    return $baseUrl . $path;
  }

  /**
   * Returns the selfservice iframe URL.
   *
   * The selfservice iframe URL is individual and secret per contract and
   * retrieved from the API using the selfServiceToken call.
   * For that reason this function requires the Billwerk API object and the
   * acting users contract ID.
   *
   * @param \Drupal\billwerk_subscriptions\Api $api
   *   The Billwerk API object.
   * @param string $contractId
   *   The contract ID.
   *
   * @throws \Drupal\billwerk_subscriptions\Exception\EnvironmentException.
   *
   * @return string
   *   The self service iframe URL.
   */
  public function getSelfserviceIframeUrl(Api $api, string $contractId): string {
    $selfServiceTokenArray = $api->getSelfserviceTokenArray($contractId);
    if (!empty($selfServiceTokenArray) && !empty($selfServiceTokenArray['Url'])) {
      return $selfServiceTokenArray['Url'];
    }
    else {
      throw new EnvironmentException('Self Service iFrame URL could not be determined for this contract.');
    }
  }

  /**
   * Get the self service public api key.
   *
   * @return string
   *   The self service public API key.
   */
  public function getSelfservicePublicApiKey(): string {
    return $this->isProduction() ? $this->settingsHelper->getProductionSelfservicePublicApiKey() : $this->settingsHelper->getSandboxSelfservicePublicApiKey();
  }

  /**
   * Get the signup iframe url.
   *
   * @param string $planVariantId
   *   The plan variant ID.
   *
   * @return string
   *   The signup URL.
   */
  public function getSignupIframeUrl(string $planVariantId): string {
    // @improve: This URL also allows prefilling other data for the user, see:
    // https://developer.billwerk.io/docs/gettingStarted/integratingBillwerk
    $baseUrl = $this->isProduction() ? self::API_PRODUCTION_SELFSERVICE_URL : self::API_SANDBOX_SELFSERVICE_URL;
    return $baseUrl . "/portal/signup.html#/{$planVariantId}";
  }

  /**
   * Returns true if the production environment is active.
   *
   * @return bool
   *   Whether the production environment is active.
   */
  public function isProduction(): bool {
    return $this->getEnvironmentName() === self::ENVIRONMENT_PRODUCTION;
  }

  /**
   * Returns true if the sandbox environment is active.
   *
   * @return bool
   *   Whether the sandbox environment is active.
   */
  public function isSandbox(): bool {
    return $this->getEnvironmentName() === self::ENVIRONMENT_SANDBOX;
  }

}

<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Utility\Error;
use Psr\Log\LogLevel;

/**
 * Helper class for easier logging within the billwerk_subscriptions module.
 */
class LogHelper {

  /**
   * The settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settingsConfig;

  /**
   * Constructs a LogHelper object.
   */
  public function __construct(
    private readonly LoggerChannelFactoryInterface $loggerFactory,
    ConfigFactoryInterface $configFactory,
  ) {
    $this->settingsConfig = $configFactory->get('billwerk_subscriptions.settings');
  }

  /**
   * Summary of logger.
   *
   * @param string $channelSuffix
   *   The channel suffix.
   *
   * @return \Drupal\Core\Logger\LoggerChannelInterface
   *   The logger channel.
   */
  public function logger(string $channelSuffix = ''): LoggerChannelInterface {
    return $this->loggerFactory->get("billwerk_subscriptions" . $channelSuffix);
  }

  /**
   * Logs a message plus additional information.
   *
   * @param string $message
   *   The message.
   * @param array $context
   *   The context.
   * @param mixed $data
   *   The data.
   * @param string $channelSuffix
   *   The channel suffix.
   * @param string $level
   *   The level.
   */
  public function log(\Stringable|string $message, array $context = [], mixed $data = NULL, string $channelSuffix = '', string $level = 'info'): void {
    $loggingLevels = $this->settingsConfig->get('logging_levels');
    if (empty($loggingLevels) || in_array($level, $loggingLevels)) {
      // Do not log, if logging is disabled (for this level)
      return;
    }

    $separator = "<br />\n";

    // Get logger.
    $logger = $this->loggerFactory->get('billwerk_subscriptions');
    $method = method_exists($logger, $level) ? $level : 'debug';

    // Amend caller info.
    $dbt = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
    $caller = isset($dbt[1]) ? $separator . 'Caller: ' . implode(':', $dbt[1]) : '';
    $message .= $caller;

    // Add data to context and message.
    if ($data) {
      $data = is_string($data) ? $data : json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_LINE_TERMINATORS);
      $context['@billwerk_subscriptions_log_data'] = $data;
      $message .= $separator . 'Data:' . $separator . '<pre>@billwerk_subscriptions_log_data</pre>';
    }

    // Log message.
    $logger->{$method}($message, $context);
  }

  /**
   * Helper function to log an exception.
   *
   * @param \Throwable $exception
   *   The exception.
   * @param string $message
   *   The message.
   * @param array $additional_variables
   *   Additional variables.
   * @param string $level
   *   The level.
   */
  public function logException(
    \Throwable $exception,
    string $message = Error::DEFAULT_ERROR_MESSAGE,
    array $additional_variables = [],
    string $level = LogLevel::ERROR,
  ): void {
    Error::logException($this->logger(), $exception, $message, $additional_variables, $level);
  }

  /**
   * Logs an emergency with additional information.
   *
   * @param \Stringable|string $message
   *   The message.
   * @param array $context
   *   The context.
   * @param mixed $data
   *   The data.
   * @param string $channelSuffix
   *   The channel suffix.
   */
  public function emergency(\Stringable|string $message, array $context = [], mixed $data = NULL, string $channelSuffix = ''): void {
    $this->log($message, $context, $data, 'emergency', $channelSuffix);
  }

  /**
   * Logs an alert with additional information.
   *
   * @param \Stringable|string $message
   *   The message.
   * @param array $context
   *   The context.
   * @param mixed $data
   *   The data.
   * @param string $channelSuffix
   *   The channel suffix.
   */
  public function alert(\Stringable|string $message, array $context = [], mixed $data = NULL, string $channelSuffix = ''): void {
    $this->log($message, $context, $data, 'alert', $channelSuffix);
  }

  /**
   * Logs a critical message with additional information.
   *
   * @param \Stringable|string $message
   *   The message.
   * @param array $context
   *   The context.
   * @param mixed $data
   *   The data.
   * @param string $channelSuffix
   *   The channel suffix.
   */
  public function critical(\Stringable|string $message, array $context = [], mixed $data = NULL, string $channelSuffix = ''): void {
    $this->log($message, $context, $data, 'critical', $channelSuffix);
  }

  /**
   * Logs an error with additional information.
   *
   * @param \Stringable|string $message
   *   The message.
   * @param array $context
   *   The context.
   * @param mixed $data
   *   The data.
   * @param string $channelSuffix
   *   The channel suffix.
   */
  public function error(\Stringable|string $message, array $context = [], mixed $data = NULL, string $channelSuffix = ''): void {
    $this->log($message, $context, $data, 'error', $channelSuffix);
  }

  /**
   * Logs a warning with additional information.
   *
   * @param \Stringable|string $message
   *   The message.
   * @param array $context
   *   The context.
   * @param mixed $data
   *   The data.
   * @param string $channelSuffix
   *   The channel suffix.
   */
  public function warning(\Stringable|string $message, array $context = [], mixed $data = NULL, string $channelSuffix = ''): void {
    $this->log($message, $context, $data, 'warning', $channelSuffix);
  }

  /**
   * Logs a notice with additional information.
   *
   * @param \Stringable|string $message
   *   The message.
   * @param array $context
   *   The context.
   * @param mixed $data
   *   The data.
   * @param string $channelSuffix
   *   The channel suffix.
   */
  public function notice(\Stringable|string $message, array $context = [], mixed $data = NULL, string $channelSuffix = ''): void {
    $this->log($message, $context, $data, 'notice', $channelSuffix);
  }

  /**
   * Logs an information with additional information.
   *
   * @param \Stringable|string $message
   *   The message.
   * @param array $context
   *   The context.
   * @param mixed $data
   *   The data.
   * @param string $channelSuffix
   *   The channel suffix.
   */
  public function info(\Stringable|string $message, array $context = [], mixed $data = NULL, string $channelSuffix = ''): void {
    $this->log($message, $context, $data, 'info', $channelSuffix);
  }

  /**
   * Logs a debug information with additional information.
   *
   * @param \Stringable|string $message
   *   The message.
   * @param array $context
   *   The context.
   * @param mixed $data
   *   The data.
   * @param string $channelSuffix
   *   The channel suffix.
   */
  public function debug(\Stringable|string $message, array $context = [], mixed $data = NULL, string $channelSuffix = ''): void {
    $this->log($message, $context, $data, 'debug', $channelSuffix);
  }

}

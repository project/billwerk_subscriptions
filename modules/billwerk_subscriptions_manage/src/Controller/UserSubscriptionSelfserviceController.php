<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions_manage\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use Drupal\billwerk_subscriptions\EmbedHelper;
use Drupal\billwerk_subscriptions\Subscriber;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for Billwerk User routes.
 */
final class UserSubscriptionSelfserviceController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('billwerk_subscriptions.embed_helper'),
      $container->get('plugin.manager.block'),
    );
  }

  /**
   * Constructs a UserSubscriptionSelfserviceController object.
   *
   * @param \Drupal\billwerk_subscriptions\EmbedHelper $embedHelper
   *   The embed helper.
   * @param \Drupal\Core\Block\BlockManagerInterface $pluginManagerBlock
   *   The block plugin manager.
   */
  public function __construct(
    protected readonly EmbedHelper $embedHelper,
    private readonly BlockManagerInterface $pluginManagerBlock,
  ) {
  }

  /**
   * Shows the self service portal for a single subscription of the given user.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   *
   * @return array
   *   The Drupal render array.
   */
  public function manageUserContract(UserInterface $user): array {
    try {
      $subscriber = Subscriber::load($user);
      $build = [];

      // Only show for the user himself. Managing other users subscriptions
      // isn't possible yet:
      if ($this->currentUser()->id() === $user->id()) {
        $config = $this->config('billwerk_subscriptions_manage.settings');
        $manageSubscriptionsLink = $config->get('page.settings.manage_subscriptions_link');
        if ($manageSubscriptionsLink == 'link') {
          $build['user_subscription_selfservice_link'] = [
            '#type' => 'link',
            '#title' => $this->t('Change my subscription plan & add-ons'),
            '#url' => Url::fromRoute('billwerk_subscriptions_manage.current_user_subscription'),
            '#attributes' => [
              'class' => [
                'button',
                'button--user-subscription-selfservice',
              ],
            ],
          ];
        }
        elseif ($manageSubscriptionsLink == 'form') {
          $config = $this->config('billwerk_subscriptions_manage.settings');
          $plugin_block = $this->pluginManagerBlock->createInstance('billwerk_subscriptions_manage_billwerk_user_subscriptions', [
            'source' => $config->get('page.settings.source'),
            'show_active_subscription' => $config->get('page.settings.show_active_subscription'),
            'show_active_components' => $config->get('page.settings.show_active_components'),
            'no_active_components_text' => $config->get('page.settings.no_active_components_text'),
            'show_change_plan_variant_button' => $config->get('page.settings.show_change_plan_variant_button'),
            'show_cancel_plan_variant_button' => $config->get('page.settings.show_cancel_plan_variant_button'),
            'show_components_subscribe_button' => $config->get('page.settings.show_components_subscribe_button'),
            'show_components_unsubscribe_button' => $config->get('page.settings.show_components_unsubscribe_button'),
            'modal_actions' => $config->get('page.settings.modal_actions'),
          ]);
          $build['user_subscription_selfservice_form'] = $plugin_block->build();
        }
      }

      // Embed Billwerk selfservice embed iframe:
      $build['user_subscription_selfservice_embed'] = $this->embedHelper->buildSelfserviceManageContractEmbed($subscriber);
    }
    catch (\Exception $e) {
      // Log exception:
      Error::logException($this->getLogger('billwerk_subscriptions_manage'), $e);
      // Show error message:
      // @improve once https://www.drupal.org/project/drupal/issues/3439618 is fixed:
      $build = [
        'message' => [
          '#theme' => 'status_messages',
          '#message_list' => [
            'error' => [
              $this->t('The website encountered an unexpected error. Try again later.'),
            ],
          ],
          '#status_headings' => [
            'error' => $this
              ->t('Error message'),
          ],
        ],
      ];
    }

    return $build;
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The acting user to run access checks for.
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The subscriber user which is being viewed.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function accessManageUserContract(AccountInterface $account, UserInterface $user) {
    // Warning: $account is the current user accessing the controller!
    // @todo Do we need a $subscriber or $user parameter here instead=
    $access = AccessResult::forbidden();
    if ($account->id() === $user->id()) {
      // Is the current users account.
      $access = AccessResult::allowedIfHasPermission($account, 'billwerk_subscriptions_selfservice_manage_own_contract');
    }
    else {
      // Is a different users account.
      $access = AccessResult::allowedIfHasPermission($account, 'billwerk_subscriptions_selfservice_manage_any_contract');
    }
    return $access->andIf(AccessResult::allowedIf(Subscriber::loadByDrupalUid((int) $user->id())->hasUserBillwerkContractId()));
  }

}

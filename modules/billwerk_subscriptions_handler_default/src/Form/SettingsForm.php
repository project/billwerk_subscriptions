<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions_handler_default\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Billwerk Subscriptions Handler: Default settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'billwerk_subscriptions_handler_default_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['billwerk_subscriptions_handler_default.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('billwerk_subscriptions_handler_default.settings');

    $form['settings'] = [
      '#type' => 'details',
      '#title' => $this->t('General settings'),
      '#open' => TRUE,
      '#description' => $this->t('General Billwerk Subscriptions Handler settings'),
      '#description_display' => 'after',
    ];

    $form['settings']['on_user_registration_create_subscription'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('On user creation create a Billwerk Subscription'),
      '#default_value' => $config->get('on_user_registration_create_subscription'),
      '#description' => $this->t('Creates a Billwerk Subscription and Customer through an order when a Drupal user is newly created.'),
    ];
    $form['settings']['billwerk_registration_plan_variant_id_sandbox'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Billwerk Plan Variant ID for registration (Sandbox)'),
      '#description' => $this->t('The Billwerk Plan Variant ID used for Billwerk Customer / Contract creation on Drupal user registration. Enter the plan variant id from the Sandbox environment here. <em>Leave empty to NOT create Billwerk Customer / Contract on Drupal user registration.</em>'),
      '#default_value' => $this->config('billwerk_subscriptions_handler_default.settings')->get('billwerk_registration_plan_variant_id_sandbox'),
      '#states' => [
        'visible' => [
          ':input[name="on_user_registration_create_subscription"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['settings']['billwerk_registration_plan_variant_id_production'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Billwerk Plan Variant ID for registration (Production)'),
      '#description' => $this->t('The Billwerk Plan Variant ID used for Billwerk Customer / Contract creation on Drupal user registration. Enter the plan variant id from the Production environment here. <em>Leave empty to NOT create Billwerk Customer / Contract on Drupal user registration.</em>'),
      '#default_value' => $this->config('billwerk_subscriptions_handler_default.settings')->get('billwerk_registration_plan_variant_id_production'),
      '#states' => [
        'visible' => [
          ':input[name="on_user_registration_create_subscription"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['settings']['drupal_events'] = [
      '#type' => 'details',
      '#title' => $this->t('Drupal events'),
      '#open' => FALSE,
      '#description' => $this->t('Reactions on Drupal events'),
      '#description_display' => 'after',
    ];
    $form['settings']['drupal_events']['on_user_login_refresh_subscription'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('On login refresh subscription'),
      '#default_value' => $config->get('on_user_login_refresh_subscription'),
      '#description' => $this->t('Refreshes the user subscription from Billwerk on Drupal user login'),
    ];

    $form['settings']['drupal_events']['on_drupal_user_block'] = [
      '#type' => 'select',
      '#title' => $this->t('On Drupal user blocked'),
      '#description' => $this->t('Reaction when a Drupal user with Billwerk Subscription is blocked'),
      '#options' => [
        '' => $this->t('Do nothing') . ' (' . $this->t('Default') . ')',
        'lock' => $this->t('Lock Customer at Billwerk'),
      ],
      '#default_value' => $config->get('on_drupal_user_block'),
    ];

    $form['settings']['drupal_events']['on_drupal_user_unblock'] = [
      '#type' => 'select',
      '#title' => $this->t('On Drupal user unblocked'),
      '#description' => $this->t('Reaction when a Drupal user with Billwerk Subscription is unblocked'),
      '#options' => [
        '' => $this->t('Do nothing') . ' (' . $this->t('Default') . ')',
        'unlock' => $this->t('Unlock Customer at Billwerk'),
      ],
      '#default_value' => $config->get('on_drupal_user_unblock'),
    ];

    $form['settings']['drupal_events']['on_drupal_user_delete'] = [
      '#type' => 'select',
      '#title' => $this->t('On Drupal user deleted'),
      '#description' => $this->t('Reaction when a Drupal user with Billwerk Subscription is deleted.'),
      '#options' => [
        '' => $this->t('Do nothing') . ' (' . $this->t('Default') . ')',
        'lock' => $this->t('Lock Customer at Billwerk'),
        'delete' => $this->t('Delete customer at Billwerk'),
      ],
      '#default_value' => $config->get('on_drupal_user_delete'),
    ];

    // Reaction on Billwerk hooks:
    $form['settings']['billwerk_events'] = [
      '#type' => 'details',
      '#title' => $this->t('Billwerk events'),
      '#open' => FALSE,
      '#description' => $this->t('Reactions on Billwerk events'),
      '#description_display' => 'after',
    ];
    $form['settings']['billwerk_events']['on_billwerk_contract_deleted'] = [
      '#type' => 'select',
      '#title' => $this->t('On Billwerk Contract deleted'),
      '#description' => $this->t('When a Billwerk Contract is deleted ("ContractDeleted" webhook).<br /><strong>Note: When the referenced Billwerk contract is deleted, the Billwerk Contract ID will always be removed from the Drupal user profile regardless of this setting, as the reference wouldn\'t make any sense anymore.</strong>'),
      '#options' => [
        '' => $this->t('Do nothing') . ' (' . $this->t('Default') . ')',
        'block' => $this->t('Block user in Drupal'),
        'delete' => $this->t('Delete user in Drupal'),
      ],
      '#default_value' => $config->get('on_billwerk_contract_deleted'),
    ];
    $form['settings']['billwerk_events']['on_billwerk_contract_cancelled'] = [
      '#type' => 'select',
      '#title' => $this->t('On Billwerk Contract canceled / annulled'),
      '#description' => $this->t('When a Billwerk Contract has been canceled / annulled ("ContractChanged" webhook with event type "EndContract, Annulation"). This is NOT called when the cancellation was requested, but when the contract changes to ended state.'),
      '#options' => [
        '' => $this->t('Do nothing') . ' (' . $this->t('Default') . ')',
        'block' => $this->t('Block user in Drupal'),
        'delete' => $this->t('Delete user in Drupal'),
      ],
      '#default_value' => $config->get('on_billwerk_contract_cancelled'),
    ];
    $form['settings']['billwerk_events']['on_billwerk_customer_deleted'] = [
      '#type' => 'select',
      '#title' => $this->t('On Billwerk Customer deleted'),
      '#description' => $this->t('When a Billwerk Customer is deleted ("CustomerDeleted" webhook)'),
      '#options' => [
        '' => $this->t('Do nothing') . ' (' . $this->t('Default') . ')',
        'block' => $this->t('Block user in Drupal'),
        'delete' => $this->t('Delete user in Drupal'),
      ],
      '#default_value' => $config->get('on_billwerk_customer_deleted'),
    ];
    $form['settings']['billwerk_events']['on_billwerk_customer_locked'] = [
      '#type' => 'select',
      '#title' => $this->t('On Billwerk Customer locked'),
      '#description' => $this->t('When a Billwerk Customer is locked ("CustomerLocked" webhook)'),
      '#options' => [
        '' => $this->t('Do nothing') . ' (' . $this->t('Default') . ')',
        'block' => $this->t('Block user in Drupal'),
      ],
      '#default_value' => $config->get('on_billwerk_customer_locked'),
    ];
    $form['settings']['billwerk_events']['on_billwerk_customer_unlocked'] = [
      '#type' => 'select',
      '#title' => $this->t('On Billwerk Customer unlocked'),
      '#description' => $this->t('When a Billwerk Customer is unlocked ("CustomerUnlocked" webhook)'),
      '#options' => [
        '' => $this->t('Do nothing') . ' (' . $this->t('Default') . ')',
        'unblock' => $this->t('Unblock user in Drupal'),
      ],
      '#default_value' => $config->get('on_billwerk_customer_unlocked'),
    ];
    $form['settings']['billwerk_events']['on_billwerk_customer_payment_escalated'] = [
      '#type' => 'select',
      '#title' => $this->t('On Billwerk payment escalated'),
      '#description' => $this->t('When a Billwerk payment escalation ("PaymentEscalated" webhook)'),
      '#options' => [
        '' => $this->t('Do nothing') . ' (' . $this->t('Default') . ')',
        'block' => $this->t('Block user in Drupal'),
      ],
      '#default_value' => $config->get('on_billwerk_customer_payment_escalated'),
    ];
    $form['settings']['billwerk_events']['on_billwerk_customer_payment_escalation_reset'] = [
      '#type' => 'select',
      '#title' => $this->t('On Billwerk payment escalation reset'),
      '#description' => $this->t('When a Billwerk payment escalation reset ("PaymentEscalationReset" webhook)'),
      '#options' => [
        '' => $this->t('Do nothing') . ' (' . $this->t('Default') . ')',
        'unblock' => $this->t('Unblock user in Drupal'),
      ],
      '#default_value' => $config->get('on_billwerk_customer_payment_escalation_reset'),
    ];

    // Other settings:
    $form['settings']['account'] = [
      '#type' => 'details',
      '#title' => $this->t('Drupal user account'),
      '#open' => FALSE,
      '#description' => $this->t('Drupal user account related settings'),
      '#description_display' => 'after',
    ];
    $form['settings']['account']['fetch_overwrite_status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Fetch and override the subscriber Drupal user status from Billwerk'),
      '#default_value' => $config->get('fetch_overwrite_status'),
      '#description' => $this->t('Fetched Billwerk value should overwrite the user account status value'),
    ];
    $form['settings']['account']['fetch_overwrite_mail'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Fetch and override the subscriber Drupal user mail from Billwerk'),
      '#default_value' => $config->get('fetch_overwrite_mail'),
      '#description' => $this->t('Fetched Billwerk value should overwrite the user account mail value'),
    ];
    $form['settings']['account']['disable_drupal_mail_edit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable email address change in user profile'),
      '#default_value' => $config->get('disable_drupal_mail_edit'),
      '#description' => $this->t('Billwerk should be the leading system for user data. To prevent different email addresses for the same account, you typically want to disable email address editing in user profiles.'),
      '#states' => [
        'visible' => [
          ':input[name="fetch_overwrite_mail"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('billwerk_subscriptions_handler_default.settings')
      ->set('on_user_registration_create_subscription', $form_state->getValue('on_user_registration_create_subscription'))
      ->set('billwerk_registration_plan_variant_id_sandbox', $form_state->getValue('billwerk_registration_plan_variant_id_sandbox'))
      ->set('billwerk_registration_plan_variant_id_production', $form_state->getValue('billwerk_registration_plan_variant_id_production'))
      ->set('on_user_login_refresh_subscription', $form_state->getValue('on_user_login_refresh_subscription'))

      ->set('on_drupal_user_block', $form_state->getValue('on_drupal_user_block'))
      ->set('on_drupal_user_unblock', $form_state->getValue('on_drupal_user_unblock'))
      ->set('on_drupal_user_delete', $form_state->getValue('on_drupal_user_delete'))

      ->set('on_billwerk_contract_deleted', $form_state->getValue('on_billwerk_contract_deleted'))
      ->set('on_billwerk_contract_cancelled', $form_state->getValue('on_billwerk_contract_cancelled'))
      ->set('on_billwerk_customer_deleted', $form_state->getValue('on_billwerk_customer_deleted'))
      ->set('on_billwerk_customer_locked', $form_state->getValue('on_billwerk_customer_locked'))
      ->set('on_billwerk_customer_unlocked', $form_state->getValue('on_billwerk_customer_unlocked'))
      ->set('on_billwerk_customer_payment_escalated', $form_state->getValue('on_billwerk_customer_payment_escalated'))
      ->set('on_billwerk_customer_payment_escalation_reset', $form_state->getValue('on_billwerk_customer_payment_escalation_reset'))

      ->set('fetch_overwrite_status', $form_state->getValue('fetch_overwrite_status'))
      ->set('fetch_overwrite_mail', $form_state->getValue('fetch_overwrite_mail'))
      ->set('disable_drupal_mail_edit', $form_state->getValue('disable_drupal_mail_edit'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}

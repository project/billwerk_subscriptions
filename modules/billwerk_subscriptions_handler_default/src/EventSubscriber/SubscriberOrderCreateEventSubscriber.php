<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions_handler_default\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\billwerk_subscriptions\Environment;
use Drupal\billwerk_subscriptions\Event\SubscriberOrderCreateEvent;
use Drupal\billwerk_subscriptions\Exception\EnvironmentException;
use Drupal\billwerk_subscriptions\Exception\SubscriberException;
use Drupal\billwerk_subscriptions\LogHelper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscriber to server-side order created events.
 *
 * Implements the individual order fields and functionality before the order
 * is submitted to Billwerk.
 */
class SubscriberOrderCreateEventSubscriber implements EventSubscriberInterface {

  /**
   * The settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Constructs a SubscriberOrderCreateEventSubscriber object.
   *
   * @param \Drupal\billwerk_subscriptions\LogHelper $logHelper
   *   The Log Helper.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The Config Factory.
   * @param \Drupal\billwerk_subscriptions\Environment $environment
   *   The Environment.
   */
  public function __construct(
    protected readonly LogHelper $logHelper,
    ConfigFactoryInterface $configFactory,
    protected readonly Environment $environment,
  ) {
    $this->config = $configFactory->get('billwerk_subscriptions_handler_default.settings');
  }

  /**
   * Handles the subscriber order create event.
   *
   * @param \Drupal\billwerk_subscriptions\Event\SubscriberOrderCreateEvent $event
   *   The subscriber order create event.
   *
   * @throws \Drupal\billwerk_subscriptions\Exception\EnvironmentException
   *   Thrown when the environment is unknown.
   * @throws \Drupal\billwerk_subscriptions\Exception\SubscriberException
   *   Thrown when the registration plan variant is empty.
   */
  public function onSubscriberOrderCreate(SubscriberOrderCreateEvent $event): void {
    if ($event->getOrderType() != SubscriberOrderCreateEvent::ORDER_TYPE_CREATE_CUSTOMER_CONTRACT) {
      // We only handle create customer + contract orders here!
      return;
    }

    // Get the order data to be sent to Billwerk:
    $orderData = $event->getOrderData();

    if ($this->environment->isProduction()) {
      $planVariantId = $this->config->get('billwerk_registration_plan_variant_id_production');
    }
    elseif ($this->environment->isSandbox()) {
      $planVariantId = $this->config->get('billwerk_registration_plan_variant_id_sandbox');
    }
    else {
      throw new EnvironmentException('Unknown environment: ' . $this->environment->getEnvironmentName());
    }
    if (empty($planVariantId)) {
      throw new SubscriberException('Registration plan variant for environment: ' . $this->environment->getEnvironmentName() . ' is empty. Order creation was not possible. Check your settings.');
    }
    // Change the order data as we need it:
    $orderData['Cart']['PlanVariantId'] = $planVariantId;
    // We don't need to change other values in $orderData, the defaults are
    // fine. Set the changed order data:
    $event->setOrderData($orderData);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      SubscriberOrderCreateEvent::class => ['onSubscriberOrderCreate'],
    ];
  }

}

<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions_handler_default\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\billwerk_subscriptions\Event\SubscriberContractChangedEvent;
use Drupal\billwerk_subscriptions\LogHelper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscriber to contract changes.
 *
 * Updates the user, whenever the Billwerk Contract changed.
 */
class SubscriberContractChangedEventSubscriber implements EventSubscriberInterface {

  /**
   * The settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Constructs a SubscriberContractChangedEventSubscriber object.
   *
   * @param \Drupal\billwerk_subscriptions\LogHelper $logHelper
   *   The log helper.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The Config Factory.
   */
  public function __construct(
    protected readonly LogHelper $logHelper,
    ConfigFactoryInterface $configFactory,
  ) {
    $this->config = $configFactory->get('billwerk_subscriptions_handler_default.settings');
  }

  /**
   * Handles the subscriber contract changed event.
   *
   * @param \Drupal\billwerk_subscriptions\Event\SubscriberContractChangedEvent $event
   *   The subscriber contract changed event.
   */
  public function onSubscriberContractChangedEvent(SubscriberContractChangedEvent $event): void {
    $subscriber = $event->getSubscriber();
    // @improve: We should sort out changes that are not relevant to the subscription, if it is possible and makes sense.
    // Trigger a refresh of the subscriber:
    $subscriber->refreshFromBillwerkContractSubscription();

    // If contract ended or was annulled:
    if (in_array($event->getChangeType(), [
      SubscriberContractChangedEvent::CONTRACT_CHANGE_TYPE_ENDCONTRACT,
      SubscriberContractChangedEvent::CONTRACT_CHANGE_TYPE_ANNULATION,
    ])) {
      // React to even as configured in the settings:
      $onBillwerkContractCanceled = $this->config->get('on_billwerk_contract_cancelled');
      if (!empty($onBillwerkContractCanceled)) {
        switch ($onBillwerkContractCanceled) {
          case 'block':
            $subscriber->getUser()->block()->save();
            $this->logHelper->info(
              'Blocked user #@uid because the contract was canceled at Billwerk and user blocking is configured as reaction in billwerk_subscriptions_handler_default.',
              [
                '@uid' => $subscriber->getUser()->id(),
              ],
              NULL, __CLASS__ . __FUNCTION__);
            break;

          case 'delete':
            // @improve: Better use user_cancel() here to respect the \Drupal::config('user.settings')->get('cancel_method');.
            // As this includes batch etc. for now we decided against this for
            // bad DX reasons.
            $subscriber->getUser()->delete();
            $this->logHelper->info(
              'Deleted user #@uid because the contract was canceled at Billwerk and user deletion is configured as reaction in billwerk_subscriptions_handler_default.',
              [
                '@uid' => $subscriber->getUser()->id(),
              ],
              NULL, __CLASS__ . __FUNCTION__);
            break;
        }
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      SubscriberContractChangedEvent::class => ['onSubscriberContractChangedEvent'],
    ];
  }

}

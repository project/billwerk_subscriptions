# billwerk_subscriptions_cache

*billwerk_subscriptions_cache* is just a possibility and idea, 
if fetching the data from
the API happens too often and is too expensive or runs into limits.

If that happens, we could add a local caching JSON field to user profiles
where we store the contract data from billwerk for a defined amount of time.

Alternative modules *subman* and *billwerk* do it that way,
 but we decided it might not be
needed and perhaps even is overhead, complicating things without much benefit.

If we're wrong, this is the place, where that submodule should be implemented.
It might then make sense to depend on the json_field module, if core doesn't
provide JSON fields then already:
```
dependencies:
  - json_field:json_field
```

<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions_entities\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the Billwerk Plan entity edit forms.
 */
final class BillwerkPlanForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);

    $message_args = ['%label' => $this->entity->toLink()->toString()];
    $logger_args = [
      '%label' => $this->entity->label(),
      'link' => $this->entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New Billwerk Plan %label has been created.', $message_args));
        $this->logger('billwerk_subscriptions_entities')->notice('New Billwerk Plan %label has been created.', $logger_args);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The Billwerk Plan %label has been updated.', $message_args));
        $this->logger('billwerk_subscriptions_entities')->notice('The Billwerk Plan %label has been updated.', $logger_args);
        break;

      default:
        throw new \LogicException('Could not save the entity.');
    }

    $form_state->setRedirectUrl($this->entity->toUrl());

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function getNewRevisionDefault() {
    // Create new revisions by default:
    return TRUE;
  }

}

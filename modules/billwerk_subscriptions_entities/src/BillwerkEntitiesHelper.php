<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions_entities;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\billwerk_subscriptions\Environment;
use Drupal\billwerk_subscriptions\Exception\EnvironmentException;
use Drupal\billwerk_subscriptions_entities\Entity\BillwerkComponent;
use Drupal\billwerk_subscriptions_entities\Entity\BillwerkPlan;
use Drupal\billwerk_subscriptions_entities\Entity\BillwerkPlanVariant;

/**
 * Helper service to determine plans for registrations and upgrades.
 */
class BillwerkEntitiesHelper {

  /**
   * Constructs a SubscriptionPlansManager object.
   */
  public function __construct(
    protected readonly EntityTypeManagerInterface $entityTypeManager,
  ) {
  }

  /**
   * Returns the Billwerk Plan-Variants.
   *
   * @param bool $enabled
   *   Filter enabled.
   * @param bool $onlyVisible
   *   Filter hidden.
   *
   * @return \Drupal\billwerk_subscriptions_entities\Entity\BillwerkPlanVariant[]
   *   The BillwerkPlanVariants[] array.
   */
  public function getBillwerkPlanVariants(bool $enabled = TRUE, $onlyVisible = TRUE): array {
    $query = $this->entityTypeManager->getStorage('billwerk_plan_variant')->getQuery()
      ->condition('status', $enabled)
      ->sort('weight', 'ASC')
      ->accessCheck(TRUE);
    if ($onlyVisible) {
      // Filter hidden (otherwise do not use this filter):
      $query->condition('hidden', FALSE);
    }
    $results = $query->execute();

    return !empty($results) ? $this->entityTypeManager->getStorage('billwerk_plan_variant')->loadMultiple($results) : [];
  }

  /**
   * Returns the BillwerkPlanVariant Entity object for the given machine_name.
   *
   * @param string $machineName
   *   The machine_name.
   *
   * @return ?\Drupal\billwerk_subscriptions_entities\Entity\BillwerkPlanVariant
   *   The BillwerkPlanVariant Entity.
   */
  public function getBillwerkPlanVariantByMachineName(string $machineName): ?BillwerkPlanVariant {
    $query = $this->entityTypeManager->getStorage('billwerk_plan_variant')->getQuery()
      // ->condition('status', 1)
      // ->condition('hidden', 0)
      ->condition('machine_name', $machineName, 'IN')
      ->range(0, 1)
      ->accessCheck(TRUE);
    $results = $query->execute();

    if (empty($results)) {
      return NULL;
    }

    return $this->entityTypeManager->getStorage('billwerk_plan_variant')->load(array_pop($results));
  }

  /**
   * Returns the BillwerkPlanVariant entity by its $billwerkPlanVariantId.
   *
   * @param string $billwerkPlanVariantId
   *   The Billwerk Plan Variant ID.
   * @param \Drupal\billwerk_subscriptions\Environment $environment
   *   The environment.
   * @param bool $enabled
   *   Filter enabled.
   * @param mixed $onlyVisible
   *   Filter hidden.
   *
   * @throws \Drupal\billwerk_subscriptions\Exception\EnvironmentException
   *
   * @return ?\Drupal\billwerk_subscriptions_entities\Entity\BillwerkPlanVariant
   *   The BillwerkPlanVariant Entity.
   */
  public function getBillwerkPlanVariantByPlanVariantId(string $billwerkPlanVariantId, Environment $environment, bool $enabled = TRUE, $onlyVisible = TRUE): ?BillwerkPlanVariant {
    $query = $this->entityTypeManager->getStorage('billwerk_plan_variant')->getQuery()
      ->condition('status', $enabled)
      ->range(0, 1)
      ->accessCheck(TRUE);
    if ($onlyVisible) {
      // Filter hidden (otherwise do not use this filter):
      $query->condition('hidden', FALSE);
    }

    if ($environment->isProduction()) {
      $query->condition('billwerk_id_production', $billwerkPlanVariantId);
    }
    elseif ($environment->isSandbox()) {
      $query->condition('billwerk_id_sandbox', $billwerkPlanVariantId);
    }
    else {
      throw new EnvironmentException('Unknown environment: "' . $environment->getEnvironmentName() . '"');
    }

    $results = $query->execute();

    if (empty($results)) {
      return NULL;
    }

    return $this->entityTypeManager->getStorage('billwerk_plan_variant')->load(array_pop($results));
  }

  /**
   * Returns all available plan variants. Should rarely be used!
   *
   * This should typically only be used for admins, as it also contains
   * options that should not be chosen by users.
   *
   * @param ?string $viewMode
   *   The viewmode to render the select options in. Entity view mode string or
   *   NULL, if the label should be used.
   * @param ?string $langcode
   *   Optional langcode. Will be auto-determined if empty.
   *
   * @return array
   *   The select options array with the plan keys as key and translated
   *   display values as value.
   */
  public function getPlanVariantsAllSelectOptions(?string $viewMode = NULL, ?string $langcode = NULL): array {
    $billwerkPlanVariants = $this->getBillwerkPlanVariants();
    return $this->planVariantsToSelectOptions($billwerkPlanVariants, $viewMode, $langcode);
  }

  /**
   * Returns the FAPI (select/radios) options for plan variant upgrades.
   *
   * @param ?string $currentPlanVariantMachineName
   *   The current plan variant key.
   * @param ?string $viewMode
   *   The viewmode to render the select options in. Entity view mode string or
   *   NULL, if the label should be used.
   * @param ?string $langcode
   *   Optional langcode. Will be auto-determined if empty.
   *
   * @return array
   *   The select options array with the plan keys as key and translated
   *   display values as value.
   */
  public function getPlanVariantsUpgradeSelectOptions(?string $currentPlanVariantMachineName = NULL, ?string $viewMode = NULL, ?string $langcode = NULL): array {
    if (empty($currentPlanVariantMachineName)) {
      return $this->getPlanVariantsAllSelectOptions($viewMode, $langcode);
    }
    else {
      $planVariant = $this->getBillwerkPlanVariantByMachineName($currentPlanVariantMachineName);
      $upgradeablePlanVariants = $planVariant ? $planVariant->getUpgradeableTo() : [];

      return $this->planVariantsToSelectOptions($upgradeablePlanVariants, $viewMode, $langcode);
    }

  }

  /**
   * Returns the FAPI (select/radios) options for plan variant downgrades.
   *
   * @param ?string $currentPlanVariantMachineName
   *   The current plan variant key.
   * @param ?string $viewMode
   *   The viewmode to render the select options in. Entity view mode string or
   *   NULL, if the label should be used.
   * @param ?string $langcode
   *   Optional langcode. Will be auto-determined if empty.
   *
   * @return array
   *   The select options array with the plan keys as key and translated
   *   display values as value.
   */
  public function getPlanVariantsDowngradeSelectOptions(?string $currentPlanVariantMachineName = NULL, ?string $viewMode = NULL, ?string $langcode = NULL): array {
    if (empty($currentPlanVariantMachineName)) {
      // No downgrade possible, if no current plan.
      return [];
    }
    else {
      $planVariant = $this->getBillwerkPlanVariantByMachineName($currentPlanVariantMachineName);
      $downgradeablePlanVariants = $planVariant->getDowngradeableTo();

      return $this->planVariantsToSelectOptions($downgradeablePlanVariants, $viewMode, $langcode);
    }
  }

  /**
   * Returns the Billwerk Plans.
   *
   * @param bool $enabled
   *   Filter enabled.
   * @param bool $onlyVisible
   *   Filter hidden.
   *
   * @return \Drupal\billwerk_subscriptions_entities\Entity\BillwerkPlan[]
   *   The BillwerkPlans[] array.
   */
  public function getBillwerkPlans(bool $enabled = TRUE, $onlyVisible = TRUE): array {
    $billwerkPlanStorage = $this->entityTypeManager->getStorage('billwerk_plan');
    $query = $billwerkPlanStorage->getQuery()
      ->condition('status', $enabled)
      ->sort('weight', 'ASC')
      ->accessCheck(TRUE);
    $results = $query->execute();
    if ($onlyVisible) {
      // Filter hidden (otherwise do not use this filter):
      $query->condition('hidden', FALSE);
    }

    return !empty($results) ? $billwerkPlanStorage->loadMultiple($results) : [];
  }

  /**
   * Returns the BillwerkPlan Entity object for the given machine_name.
   *
   * @param string $machineName
   *   The machine_name.
   *
   * @return ?\Drupal\billwerk_subscriptions_entities\Entity\BillwerkPlan
   *   The BillwerkPlan Entity.
   */
  public function getBillwerkPlanByMachineName(string $machineName): ?BillwerkPlan {
    $billwerkPlanStorage = $this->entityTypeManager->getStorage('billwerk_plan');
    $query = $billwerkPlanStorage->getQuery()
      // ->condition('status', 1)
      // ->condition('hidden', 0)
      ->condition('machine_name', $machineName, 'IN')
      ->range(0, 1)
      ->accessCheck(TRUE);
    $results = $query->execute();

    if (empty($results)) {
      return NULL;
    }

    return $billwerkPlanStorage->load(array_pop($results));
  }

  /**
   * Returns the BillwerkPlan entity by its $billwerkPlanId.
   *
   * @param string $billwerkPlanId
   *   The Billwerk Plan ID.
   * @param \Drupal\billwerk_subscriptions\Environment $environment
   *   The environment.
   * @param bool $enabled
   *   Filter query enabled.
   * @param mixed $onlyVisible
   *   Filter query hidden.
   *
   * @throws \Drupal\billwerk_subscriptions\Exception\EnvironmentException
   *
   * @return ?\Drupal\billwerk_subscriptions_entities\Entity\BillwerkPlan
   *   The BillwerkPlan Entity.
   */
  public function getBillwerkPlanByPlanId(string $billwerkPlanId, Environment $environment, bool $enabled = TRUE, $onlyVisible = TRUE): ?BillwerkPlan {
    $billwerkPlanStorage = $this->entityTypeManager->getStorage('billwerk_plan');
    $query = $billwerkPlanStorage->getQuery()
      ->condition('status', $enabled)
      ->range(0, 1)
      ->accessCheck(TRUE);
    if ($onlyVisible) {
      // Filter hidden (otherwise do not use this filter):
      $query->condition('hidden', FALSE);
    }

    if ($environment->isProduction()) {
      $query->condition('billwerk_id_production', $billwerkPlanId);
    }
    elseif ($environment->isSandbox()) {
      $query->condition('billwerk_id_sandbox', $billwerkPlanId);
    }
    else {
      throw new EnvironmentException('Unknown environment: "' . $environment->getEnvironmentName() . '"');
    }

    $results = $query->execute();

    if (empty($results)) {
      return NULL;
    }

    return $billwerkPlanStorage->load(array_pop($results));
  }

  /**
   * Returns the Billwerk Plan's PlanVariants by $planMachineName.
   *
   * @param mixed $planMachineName
   *   The plan machine name.
   *
   * @return \Drupal\billwerk_subscriptions_entities\Entity\BillwerkPlanVariant[]
   *   The BillwerkPlanVariant array.
   */
  public function getPlanPlanVariantsByMachineName($planMachineName): array {
    $billwerkPlanStorage = $this->entityTypeManager->getStorage('billwerk_plan_variant');
    $query = $billwerkPlanStorage->getQuery()
      ->condition('status', 1)
      ->condition('hidden', 0)
      ->condition('field_billwerk_plan.entity:billwerk_plan:machine_name', $planMachineName)
      ->sort('weight', 'ASC')
      ->accessCheck(TRUE);
    $results = $query->execute();
    $billwerkPlanVariants = !empty($results) ? $billwerkPlanStorage->loadMultiple($results) : [];

    return $billwerkPlanVariants;
  }

  /*
   * ----------------- Components: ----------------------:
   */

  /**
   * Returns the BillwerkComponent entities.
   *
   * @param bool $enabled
   *   Filter enabled.
   * @param bool $onlyVisible
   *   Filter hidden.
   *
   * @return \Drupal\billwerk_subscriptions_entities\Entity\BillwerkComponent[]
   *   The BillwerkComponents[] array.
   */
  public function getBillwerkComponents(bool $enabled = TRUE, $onlyVisible = TRUE): array {
    $billwerkComponentStorage = $this->entityTypeManager->getStorage('billwerk_component');
    $query = $billwerkComponentStorage->getQuery()
      ->condition('status', $enabled)
      ->sort('weight', 'ASC')
      ->accessCheck(TRUE);
    if ($onlyVisible) {
      // Filter hidden (otherwise do not use this filter):
      $query->condition('hidden', FALSE);
    }
    $results = $query->execute();

    return !empty($results) ? $billwerkComponentStorage->loadMultiple($results) : [];
  }

  /**
   * Returns the BillwerkComponent Entity object for the given machine_name.
   *
   * @param string $machineName
   *   The machine_name.
   *
   * @return ?\Drupal\billwerk_subscriptions_entities\Entity\BillwerkComponent
   *   The BillwerkComponent Entity.
   */
  public function getBillwerkComponentByMachineName(string $machineName): ?BillwerkComponent {
    $billwerkComponentStorage = $this->entityTypeManager->getStorage('billwerk_component');
    $query = $billwerkComponentStorage->getQuery()
      ->condition('machine_name', $machineName, 'IN')
      ->range(0, 1)
      ->accessCheck(TRUE);
    $results = $query->execute();

    if (empty($results)) {
      return NULL;
    }

    return $billwerkComponentStorage->load(array_pop($results));
  }

  /**
   * Returns the BillwerkComponent by $billwerkComponentId and $environment.
   *
   * @param string $billwerkComponentId
   *   The Billwerk Component ID.
   * @param \Drupal\billwerk_subscriptions\Environment $environment
   *   The environment.
   * @param bool $enabled
   *   Filter query enabled.
   * @param mixed $onlyVisible
   *   Filter query hidden.
   *
   * @throws \Drupal\billwerk_subscriptions\Exception\EnvironmentException
   *
   * @return ?\Drupal\billwerk_subscriptions_entities\Entity\BillwerkComponent
   *   The BillwerkComponent array.
   */
  public function getBillwerkComponentByComponentId(string $billwerkComponentId, Environment $environment, bool $enabled = TRUE, $onlyVisible = TRUE): ?BillwerkComponent {
    $billwerkComponentStorage = $this->entityTypeManager->getStorage('billwerk_component');
    $query = $billwerkComponentStorage->getQuery()
      ->condition('status', $enabled)
      ->range(0, 1)
      ->accessCheck(TRUE);
    if ($onlyVisible) {
      // Filter hidden (otherwise do not use this filter):
      $query->condition('hidden', FALSE);
    }

    if ($environment->isProduction()) {
      $query->condition('billwerk_id_production', $billwerkComponentId);
    }
    elseif ($environment->isSandbox()) {
      $query->condition('billwerk_id_sandbox', $billwerkComponentId);
    }
    else {
      throw new EnvironmentException('Unknown environment: "' . $environment->getEnvironmentName() . '"');
    }

    $results = $query->execute();

    if (empty($results)) {
      return NULL;
    }

    return $billwerkComponentStorage->load(array_pop($results));
  }

  /**
   * Returns the FAPI (select/radios) options for Billwerk Components.
   *
   * @param ?string $viewMode
   *   The viewmode to render the select options in. Entity view mode string or
   *   NULL, if the label should be used.
   * @param ?string $langcode
   *   Optional langcode. Will be auto-determined if empty.
   *
   * @return array
   *   The select options array with the plan keys as key and translated
   *   display values as value.Undocumented function
   */
  public function getComponentsAllSelectOptions(?string $viewMode = NULL, ?string $langcode = NULL): array {
    $billwerkComponents = $this->getBillwerkComponents(TRUE, TRUE);
    return $this->componentsToSelectOptions($billwerkComponents, $viewMode, $langcode);
  }

  /**
   * Returns the FAPI (select/radios) options for component additions.
   *
   * @param array $currentComponentsMachineNames
   *   The current addon machine names array.
   * @param ?string $viewMode
   *   The viewmode to render the select options in. Entity view mode string or
   *   NULL, if the label should be used.
   * @param ?string $langcode
   *   Optional langcode. Will be auto-determined if empty.
   *
   * @return array
   *   The select options array with the plan keys as key and translated
   *   display values as value.Undocumented function
   */
  public function getComponentsSubscribeSelectOptions(array $currentComponentsMachineNames, ?string $viewMode = NULL, ?string $langcode = NULL): array {
    if (empty($currentComponentsMachineNames)) {
      return $this->getComponentsAllSelectOptions($viewMode, $langcode);
    }
    else {
      $billwerkComponentStorage = $this->entityTypeManager->getStorage('billwerk_component');
      $query = $billwerkComponentStorage->getQuery()
        ->condition('status', TRUE)
        ->condition('hidden', FALSE)
        ->condition('machine_name', $currentComponentsMachineNames, 'NOT IN')
        ->sort('weight', 'ASC')
        ->accessCheck(TRUE);
      $results = $query->execute();
      $billwerkPlanVariants = !empty($results) ? $billwerkComponentStorage->loadMultiple($results) : [];
      return $this->componentsToSelectOptions($billwerkPlanVariants, $viewMode, $langcode);
    }
  }

  /**
   * Returns the FAPI (select/radios) options for component removal.
   *
   * @param array $currentComponentsMachineNames
   *   The current addon machine names array.
   * @param ?string $viewMode
   *   The viewmode to render the select options in. Entity view mode string or
   *   NULL, if the label should be used.
   * @param ?string $langcode
   *   Optional langcode. Will be auto-determined if empty.
   *
   * @return array
   *   The select options array with the plan keys as key and translated
   *   display values as value.Undocumented function
   */
  public function getComponentsUnsubscribeSelectOptions(array $currentComponentsMachineNames, ?string $viewMode = NULL, ?string $langcode = NULL): array {
    if (empty($currentComponentsMachineNames)) {
      // If empty there's nothing to unsubscribe from:
      return [];
    }
    $billwerkComponentStorage = $this->entityTypeManager->getStorage('billwerk_component');
    $query = $billwerkComponentStorage->getQuery()
      ->condition('status', TRUE)
      ->condition('hidden', FALSE)
      ->condition('machine_name', $currentComponentsMachineNames, 'IN')
      ->sort('weight', 'ASC')
      ->accessCheck(TRUE);
    $results = $query->execute();
    $billwerkPlanVariants = !empty($results) ? $billwerkComponentStorage->loadMultiple($results) : [];
    return $this->componentsToSelectOptions($billwerkPlanVariants, $viewMode, $langcode);
  }

  /**
   * Turns the given Billwerk plan variants into select options.
   *
   * @param \Drupal\billwerk_subscriptions_entities\Entity\BillwerkPlanVariant[] $billwerkPlanVariants
   *   The array of BillwerkPlanVariants to turn into select options.
   * @param ?string $viewMode
   *   The viewmode to render the select options in. Entity view mode string or
   *   NULL, if the label should be used.
   * @param ?string $langcode
   *   Optional langcode. Will be auto-determined if empty.
   *
   * @return array
   *   The FAPI array.
   */
  protected function planVariantsToSelectOptions(array $billwerkPlanVariants, ?string $viewMode = NULL, ?string $langcode = NULL): array {
    $options = [];
    foreach ($billwerkPlanVariants as $billwerkPlanVariant) {
      /** @var \Drupal\billwerk_subscriptions_entities\Entity\BillwerkPlanVariant $billwerkPlanVariant */
      if ($viewMode === NULL) {
        $value = $billwerkPlanVariant->labelTranslated($langcode);
      }
      else {
        $value = $this->entityTypeManager->getViewBuilder('billwerk_plan_variant')->view($billwerkPlanVariant, $viewMode);
      }
      $options[$billwerkPlanVariant->getMachineName()] = $value;
    }
    return $options;
  }

  /**
   * Turns the given Billwerk components into select options.
   *
   * @param \Drupal\billwerk_subscriptions_entities\Entity\BillwerkComponent[] $billwerkComponents
   *   The array of BillwerkPlanVariants to turn into select options.
   * @param ?string $viewMode
   *   The viewmode to render the select options in. Entity view mode string or
   *   NULL, if the label should be used.
   * @param ?string $langcode
   *   Optional langcode. Will be auto-determined if empty.
   *
   * @return array
   *   The FAPI array.
   */
  protected function componentsToSelectOptions(array $billwerkComponents, ?string $viewMode = NULL, ?string $langcode = NULL): array {
    $options = [];
    foreach ($billwerkComponents as $billwerkComponent) {
      /** @var \Drupal\billwerk_subscriptions_entities\Entity\BillwerkComponent $billwerkComponent */
      if ($viewMode === NULL) {
        $value = $billwerkComponent->labelTranslated($langcode);
      }
      else {
        $value = $this->entityTypeManager->getViewBuilder('billwerk_plan_variant')->view($billwerkComponent, $viewMode);
      }

      $options[$billwerkComponent->getMachineName()] = $value;
    }
    return $options;
  }

}

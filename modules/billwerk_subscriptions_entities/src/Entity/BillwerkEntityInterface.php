<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions_entities\Entity;

use Drupal\Core\Session\AccountProxyInterface;

/**
 * The BillwerkEntity interface.
 */
interface BillwerkEntityInterface {

  /**
   * Returns the translated label.
   *
   * If no $langcode is given, uses LanguageInterface::TYPE_CONTENT as language.
   *
   * @param string $langcode
   *   The optional langcode.
   *
   * @return string
   *   The translated label.
   */
  public function labelTranslated(?string $langcode = NULL): string;

  /**
   * Returns the machine name of this Billwerk entity.
   *
   * @return string
   *   Machine name
   */
  public function getMachineName(): string;

  /**
   * Tells of this Billwerk entity is enabled.
   *
   * @return bool
   *   Is enabled?
   */
  public function isEnabled(): bool;

  /**
   * Tells if this Billwerk entity is hidden and should bot be bookable.
   *
   * @return bool
   *   Is hidden?
   */
  public function isHidden(): bool;

  /**
   * Returns the weight of this Billwerk entity for lists.
   *
   * @return int
   *   The weight.
   */
  public function getWeight(): int;

  /**
   * Returns the internal notes.
   *
   * @return string
   *   The internal notes.
   */
  public function getNotesInternal(): string;

  /**
   * Return the subtitle.
   *
   * @return string
   *   The subtitle.
   */
  public function getSubtitle(): string;

  /**
   * Returns the Billwerk ID for this Billwerk entity by environment.
   *
   * @param string $environment
   *   The environment key: sandbox|production.
   *
   * @return string
   *   The Billwerk ID.
   */
  public function getBillwerkId(string $environment): string;

  /**
   * Helper function to get the billwerk data.
   *
   * @param string $environment
   *   The environment: sandbox|production.
   *
   * @return array
   *   The Billwerk data array.
   */
  public function getBillwerkProductInfo(?string $environment = NULL): array;

  /**
   * Helper function to build the upgrade button as Drupal Render array.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $user
   *   The user to perform the operation.
   * @param bool $modal
   *   Show as modal?
   * @param array $additionalClasses
   *   Additional classes to add.
   *
   * @return array
   *   The Drupal render array.
   */
  public function buildSubscribeButton(
    AccountProxyInterface $user,
    bool $modal = FALSE,
    array $additionalClasses = ['secondary', 'expanded'],
  ): array;

}

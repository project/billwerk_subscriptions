<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions_entities\Entity;

use Drupal\user\Entity\Role;

/**
 * The BillwerkFeeEntity interface.
 */
interface BillwerkFeeEntityInterface extends BillwerkEntityInterface {

  /**
   * Returns the recurring fee price in cents per period.
   *
   * @param string $environment
   *   The environment key: sandbox|production.
   *
   * @return int|null
   *   The price in cents or NULL if not recurring.
   */
  public function getBillwerkProductInfoRecurringPriceCents(?string $environment = NULL): int;

  /**
   * Returns the recurring period unit: Day, Week, Month, Year.
   *
   * @param string $environment
   *   The environment key: sandbox|production.
   *
   * @return string|null
   *   The period unit or NULL if not recurring.
   */
  public function getBillwerkProductInfoRecurringPeriodUnit(?string $environment = NULL): ?string;

  /**
   * Returns the recurring period quantity.
   *
   * @param string $environment
   *   The environment key: sandbox|production.
   *
   * @return int
   *   The period quantity or NULL if not recurring.
   */
  public function getBillwerkProductInfoRecurringPeriodQuantity(?string $environment = NULL): ?int;

  /**
   * Returns the recurring fee price in cents per month.
   *
   * @param string $environment
   *   The environment key: sandbox|production.
   *
   * @return int|null
   *   The price in cents or NULL if not recurring.
   */
  public function getBillwerkProductInfoRecurringPricePerMonthCents(?string $environment = NULL): int;

  /**
   * Returns the currency ISO code, e.g. 'EUR'.
   *
   * @param string $environment
   *   The environment key: sandbox|production.
   *
   * @return string
   *   The currency ISO code.
   */
  public function getBillwerkProductInfoCurrencyCodeIso(?string $environment = NULL): string;

  /**
   * Returns the currency symbol, e.g. '€'.
   *
   * @param string $environment
   *   The environment key: sandbox|production.
   *
   * @return string
   *   The currency symbol.
   */
  public function getBillwerkProductInfoCurrencySymbol(?string $environment = NULL): string;

  /**
   * Returns the payment period mode, e.g.: PrePaid, ...
   *
   * @param string $environment
   *   The environment key: sandbox|production.
   *
   * @return string
   *   The payment period mode.
   */
  public function getBillwerkProductInfoPaymentPeriodMode(?string $environment = NULL): string;

  /**
   * Returns the role.
   *
   * @return \Drupal\user\Entity\Role
   *   The role.
   */
  public function getRole(): Role;

  /**
   * Returns the role id.
   *
   * @return string
   *   The role id.
   */
  public function getRoleId(): string;

}

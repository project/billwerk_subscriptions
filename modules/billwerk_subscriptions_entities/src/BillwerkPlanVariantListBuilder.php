<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions_entities;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Provides a list controller for the billwerk plan variant entity type.
 */
final class BillwerkPlanVariantListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['id'] = $this->t('ID');
    $header['label'] = $this->t('Label');
    $header['machine_name'] = $this->t('Machine name');
    // $header['field_subtitle'] = $this->t('Subtitle');
    $header['status'] = $this->t('Status');
    $header['hidden'] = $this->t('Hidden');
    $header['field_billwerk_plan'] = $this->t('Plan');
    $header['billwerk_id_production'] = $this->t('Billwerk ID (Production)');
    $header['billwerk_id_sandbox'] = $this->t('Billwerk ID (Sandbox)');
    $header['weight'] = $this->t('Weight');
    $header['role'] = $this->t('Role');
    // $header['field_features'] = $this->t('Features');
    // $header['description'] = $this->t('Description');
    // $header['field_plan_variant_upgr_to'] = $this->t('Upgradeable to');
    // $header['field_plan_variant_downgr_to'] = $this->t('Downgradeable to');
    // $header['uid'] = $this->t('Author');
    // $header['created'] = $this->t('Created');
    // $header['changed'] = $this->t('Updated');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\billwerk_subscriptions_entities\BillwerkPlanVariant $entity */
    $row['id'] = $entity->id();
    $row['label'] = $entity->toLink();
    $row['machine_name'] = $entity->get('machine_name')->value;
    // $row['field_subtitle'] = $entity->get('field_subtitle')->value;
    $row['status'] = $entity->get('status')->value ? $this->t('Enabled') : $this->t('Disabled');
    $row['hidden'] = $entity->get('hidden')->value ? $this->t('Hidden') : $this->t('Visible');
    $row['field_billwerk_plan'] = $entity->get('field_billwerk_plan')->entity ? $entity->get('field_billwerk_plan')->entity->label() : '-';
    $row['billwerk_id_production'] = $entity->get('billwerk_id_production')->value;
    $row['billwerk_id_sandbox'] = $entity->get('billwerk_id_sandbox')->value;
    $row['weight'] = $entity->get('weight')->value ?? '-';
    $row['role'] = $entity->get('role')->value ?? '-';
    // @codingStandardsIgnoreStart
    // $row['field_features'] = $entity->get('field_features')->getEntity() ? $entity->get('field_features')->getEntity()->label() : '-';
    // $row['description'] = $entity->get('description')->value ?: '';
    // $row['field_plan_variant_upgr_to'] = $entity->get('field_plan_variant_upgr_to')->getEntity() ? $entity->get('field_plan_variant_upgr_to')->getEntity()->label() : '-';
    // $row['field_plan_variant_downgr_to'] = $entity->get('field_plan_variant_downgr_to')->getEntity() ? $entity->get('field_plan_variant_downgr_to')->getEntity()->label() : '-';
    $row['status'] = $entity->get('status')->value ? $this->t('Enabled') : $this->t('Disabled');
    // $username_options = [
    //   'label' => 'hidden',
    //   'settings' => ['link' => $entity->get('uid')->entity->isAuthenticated()],
    // ];
    // $row['uid']['data'] = $entity->get('uid')->view($username_options);
    // $row['created']['data'] = $entity->get('created')->view(['label' => 'hidden']);
    // $row['changed']['data'] = $entity->get('changed')->view(['label' => 'hidden']);
    // @codingStandardsIgnoreEnd
    return $row + parent::buildRow($entity);
  }

}

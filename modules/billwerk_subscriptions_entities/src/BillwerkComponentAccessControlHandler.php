<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions_entities;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * The billwerk component access control handler.
 *
 * Defines the access control handler for the billwerk component (add-on) entity
 * type.
 *
 * phpcs:disable Drupal.Arrays.Array.LongLineDeclaration
 *
 * @see https://www.drupal.org/project/coder/issues/3185082
 */
final class BillwerkComponentAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResult {
    return match($operation) {
      'view' => AccessResult::allowedIfHasPermissions($account, ['view billwerk_component', 'administer billwerk_component'], 'OR'),
      'update' => AccessResult::allowedIfHasPermissions($account, ['edit billwerk_component', 'administer billwerk_component'], 'OR'),
      'delete' => AccessResult::allowedIfHasPermissions($account, ['delete billwerk_component', 'administer billwerk_component'], 'OR'),
      default => AccessResult::neutral(),
    };
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResult {
    return AccessResult::allowedIfHasPermissions($account, ['create billwerk_component', 'administer billwerk_component'], 'OR');
  }

}
